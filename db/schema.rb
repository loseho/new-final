# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160906173737) do

  create_table "candidates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.string   "gender"
    t.string   "phone"
    t.string   "address"
    t.boolean  "blacklist"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "commenter"
    t.text     "body",         limit: 65535
    t.integer  "candidate_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["candidate_id"], name: "index_comments_on_candidate_id", using: :btree
  end

  create_table "progressions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "candidate_id"
    t.string   "status"
    t.string   "stage"
    t.string   "position"
    t.float    "pre_salary",           limit: 24
    t.float    "expect_salary",        limit: 24
    t.string   "source"
    t.string   "source_detail"
    t.float    "general_test",         limit: 24
    t.string   "general_test_checker"
    t.float    "tech_test",            limit: 24
    t.string   "tech_test_checker"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["candidate_id"], name: "index_progressions_on_candidate_id", using: :btree
  end

  create_table "staffs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.string   "gender"
    t.string   "phone"
    t.string   "position"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "comments", "candidates"
  add_foreign_key "progressions", "candidates"
end
