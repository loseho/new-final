class CreateCandidates < ActiveRecord::Migration[5.0]
  def change
    create_table :candidates do |t|
      t.string :name
      t.string :email
      t.string :gender
      t.string :phone
      t.string :address
      t.boolean :blacklist

      t.timestamps
    end
  end
end