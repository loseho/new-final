class CreateProgressions < ActiveRecord::Migration[5.0]
  def change
    create_table :progressions do |t|
      t.references :candidate, foreign_key: true

      t.string :status
      t.string :stage
      t.string :position
      t.float :pre_salary
      t.float :expect_salary
      t.string :source
      t.string :source_detail
      t.float :general_test
      t.string :general_test_checker
      t.float :tech_test
      t.string :tech_test_checker

      t.timestamps
    end
  end
end
