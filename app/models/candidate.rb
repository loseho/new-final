class Candidate < ApplicationRecord
        has_many :comments, dependent: :destroy
        has_many :progressions, dependent: :destroy

        validates :name,  presence: true, length: { maximum: 50 }
        validates :email, presence: true, length: { maximum: 255 }
        validates :gender, presence: true
        validates :phone, presence: true
        validates :address, presence: true, length: { maximum: 255 }

end
