class ProgressionsController < ApplicationController

  def create
    @candidate = Candidate.find(params[:candidate_id])
    @progression = @candidate.progressions.create(progression_params)
    redirect_to candidate_path(@candidate)
  end

  def destroy
    @candidate = Candidate.find(params[:candidate_id])
    @progression = @candidate.progressions.find(params[:id])
    @progression.destroy
    redirect_to candidate_path(@candidate)
  end

  def progression_params
     params.require(:progression).permit(:status, :stage, :position, :pre_salary, :expect_salary, :source, :source_detail, :general_test,
                                          :general_test_checker, :tech_test ,:tech_test_checker)
  end
end











  # def index
  #   @progressions = Progression.all
  # end
  #
  # def new
  #   @candidate = Candidate.find(params[:candidate_id])
  #   @progression = Progression.new
  # end
  #
  # def show
  #   @progression = Progression.find(params[:id])
  # end
  #
  # def create
  #   # @candidate = Candidate.find(params[:candidate_id])
  #   @progression = Progression.new(progression_params)
  #
  #   if @progression.save
  #     redirect_to candidate_path(@candidate)
  #   else
  #     render 'new'
  #   end
  # end
  #
  # def update
  #   @candidate = Candidate.find(params[:id])
  #   @progression = Progression.find(params[:id])
  #
  #   if @progression.update(candidate_params)
  #     redirect_to candidates_url
  #   else
  #     # render 'show'
  #   end
  # end
  #
  # def destroy
  #   @progression = Progression.find(params[:id])
  #   @progression.destroy
  #   redirect_to candidates_url
  # end
  #
  # private
  # # def progression_params
  # #   params.require(:progression).permit(:status, :stage, :position, :pre_salary, :expect_salary, :source, :source_detail, :general_test,
  # #                                        :general_test_checker, :tech_test ,:tech_test_checker)
  # def progression_params
  #     params.require(:progression).permit(:status)
  # end


