class StaffsController < ApplicationController

  def index
    @staffs = Staff.all
  end

  def new
    @staff = Staff.new
  end

  def show
    @staff = Staff.find(params[:id])
  end

  def create
    @staff = Staff.new(staff_params)

    if @staff.save
      redirect_to staffs_url
    else
      render 'new'
    end
  end

  def update
    @staff = Staff.find(params[:id])

    if @staff.update(staff_params)
      redirect_to staffs_url
    else
      render 'show'
    end
  end

  def destroy
    @staff = Staff.find(params[:id])
    @staff.destroy
    redirect_to staffs_url
  end

  private
  def staff_params
    params.require(:staff).permit(:name, :email, :phone, :gender )
  end

end
