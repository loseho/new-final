class CandidatesController < ApplicationController

  def paget
  end

  def index
    @candidates = Candidate.all
  end

  def new
    @candidate = Candidate.new
  end

  def show
    @candidate = Candidate.find(params[:id])
  end

  def create
    @candidate = Candidate.new(candidate_params)

    if @candidate.save
      redirect_to candidates_url
    else
      render 'new'
    end
  end

  def update
    @candidate = Candidate.find(params[:id])

    if @candidate.update(candidate_params)
      redirect_to candidates_url
    else
      render 'show'
    end
  end

  def destroy
    @candidate = Candidate.find(params[:id])
    @candidate.destroy
    redirect_to candidates_url
  end

  private
  def candidate_params
    params.require(:candidate).permit(:name, :gender, :email, :phone,
                                      :address ,:source ,:source_detail ,:blacklist)
  end

end
