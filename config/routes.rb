Rails.application.routes.draw do

  resources :candidates do
    resources :comments
    resources :progressions
  end

  resources :staffs

  get  '/aa/paget',  to: 'candidates#paget'
  post '/aa/create', to: 'candidates#create_aa'

  root 'candidates#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
